var clientesObtenidos;

function getClients(){
  var url="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200 ) {
      console.log(request.responseText);
      clientesObtenidos=request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla= document.getElementById("tablaClientes");
  for ( var i =0; i<JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var nuevaColumna1 = document.createElement("td");
    nuevaColumna1.innerText = JSONClientes.value[i].CustomerID;
    var nuevaColumna2 = document.createElement("td");
    nuevaColumna2.innerText = JSONClientes.value[i].CompanyName;
    var nuevaColumna3 = document.createElement("td");
    var oImg = document.createElement("img");
    if (JSONClientes.value[i].Country=="UK") {
      oImg.setAttribute('src', 'https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png');  
    } else {
      oImg.setAttribute('src', 'https://www.countries-ofthe-world.com/flags-normal/flag-of-' + JSONClientes.value[i].Country + '.png');
    }
    oImg.classList.add("flag");
    nuevaColumna3.innerText = oImg;
    nuevaFila.appendChild(nuevaColumna1);
    nuevaFila.appendChild(nuevaColumna2);
    nuevaFila.appendChild(oImg);
    tabla.appendChild(nuevaFila);
  }
}
